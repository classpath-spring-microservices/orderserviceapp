# Setting up DiscoveryClient for OrderService application
1. Setting up the dependency
```xml
 <dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```
2. Create a `bootstrap.yaml` file inside `src/main/resources` directory
3. Configure the `spring.application.name`, `spring.profiles.active` and `spring.cloud.config.uri` to point to config server
```yaml
spring:
  application:
    name: orderservice
  profiles:
    active: qa
  cloud:
    config:
      uri: http://localhost:8888
```
4. Move all the configurations from `src/main/resources` to `orderservice` under `gitlab` which is managed by `config server`
5. In the `application.properties` add the below configuration specific to Eureka server
```properties
eureka.instance.preferIpAddress = true
eureka.client.registerWithEureka = true
eureka.client.fetchRegistry = true
eureka.client.serviceUrl.defaultZone = http://localhost:8761/eureka/
server.port=8222
```

6. Start the `Config server`, `Eureka Server` and `OrderService` application. You should be able to access the application pointing to `QA` profile

7. Verify if the `ORDERSERVICE` is showing up in the `Eureka` dashboard

8. Hit the `http://localhost:8761/eureka/apps` in the browser and you should see the details in `json` format

## Lab on Eureka Client and Service Discover
1. In the `OrderServieApp` add the following dependencies
```xml
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-openfeign</artifactId>
    </dependency>
```    
2. Add the `@EnableFeignClients` annotation in the ROOT class
3. Create an interface called `InventoryFeignClient` with the following code
```java
@FeignClient("inventoryservice")
public interface InventoryFeignClient {

    @GetMapping(value = "/api/v1/inventory", consumes = APPLICATION_JSON_VALUE)
    public int fetchItemsCount();

    @PostMapping(value = "/api/v1/inventory", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> updateQty();
}
```

4. Inject the `InventoryFeignClient` to `OrderServiceImpl` class
```java
    private InventoryFeignClient inventoryFeignClient;
```

5. Invoke the `InventoryService` using the `FeignClient`
```java
    public int invokeInventoryServiceUsingFeignClient(){
        log.info("Came inside the fetchInventory method");
        this.inventoryFeignClient.updateQty();
        int counter =  this.inventoryFeignClient.fetchItemsCount();
        System.out.println("Fetching the counter value using Feign client ::"+ counter);
        return counter;
    }
```

6. Verify if the `OrderService` is able to invoke the `InventoryService`

```
POST Request - http://localhost:8222/api/v1/orders
```
```json
{
        "totalPrice": 40000,
        "orderDate": "2020-06-16",
        "lineItems": [
            {
                "qty": 1,
                "price": 55000
            }
        ]
    }
```
7. Check the logs of `OrderServiceApp`
```
2020-12-22 09:59:59.537  INFO 1068 --- [nio-8222-exec-1] c.s.s.service.OrderServiceImpl           : Came inside the fetchInventory method
Fetching the counter value using Feign client ::9986
2020-12-22 10:00:10.481  INFO 1068 --- [nio-8222-exec-4] c.s.s.service.OrderServiceImpl           : Came inside the fetchInventory method
Fetching the counter value using Feign client ::9986
2020-12-22 10:00:13.838  INFO 1068 --- [nio-8222-exec-3] c.s.s.service.OrderServiceImpl           : Came inside the fetchInventory method
Fetching the counter value using Feign client ::9986
```

## Lab on updating the configuration
1. Enable the `shutdown` endpoint
```yaml
management:
  endpoint:
    shutdown:
      enabled: true
``` 
2. Update the configuration in the `Config Server`
2. Shutdown the service by invoking the `POST` request on 
```
http://localhost:82222/actuator/shutdown
```

## Lab for Resiliency

1. Add the resilicency4j dependencies in `OrderServiceApp`

```xml
<dependency>
    <groupId>io.github.resilience4j</groupId>
    <artifactId>resilience4j-spring-boot2</artifactId>
    <version>1.3.0</version>
</dependency>
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```

2. On top of the `OrderServiceImpl#saveOrder()` method add the `@CircuitBreaker` annotation

3. Add the below configuration 

```yaml
resilience4j:
  circuitbreaker:
    instances:
      orderservice:
        register-health-indicator: true
        ring-buffer-size-in-closed-state: 5
        ring-buffer-size-in-half-open-state: 3
        wait-duration-in-open-state: 30s
        failure-rate-threshold: 50
        record-exceptions:
          - java.io.IOException
          - java.util.concurrent.TimeoutException
          - org.springframework.web.client.ResourceAccessException
```

4. Pass in a fallback implementation which will be returned to the client during `OPEN` and `HALF_OPEN` state

5. The fallback method should have the same signature as of the circuit breaker method

6. The fallback method will also get additional  `Throwable` argument

7. For retries, add the below dependencies 
```xml
<dependency>
    <groupId>io.github.resilience4j</groupId>
    <artifactId>resilience4j-retry</artifactId>
    <version>1.3.0</version>
</dependency>
```

8. Add the below configurations in `application.yaml` file
```yaml
resilience4j:
  retry:
      instances:
        orderserviceRetry:
          max-retry-attempts: 4
          wait-duration: 5000
          retry-exceptions:
            - java.io.IOException
            - java.util.concurrent.TimeoutException
            - org.springframework.web.client.ResourceAccessException
``` 

9. Add the `@Retry` annotation and pass the fallback method definition in the `OrderServiceImpl#saveOrder` implementation
```java
 @Retry(name = "orderserviceRetry", fallbackMethod = "fallBackImplementation")
```
10. You can monitor the stats of the circuit breaker with the below actuator endpoint
```
http://localhost:8222/actuator/health
```

11. First bring up both the `OrderService app` and the `Inventory app`

12. In the Postman client, make a `POST` request to 
```
http://localhost:8222/api/v1/orders
{
        "totalPrice": 40000,
        "orderDate": "2020-06-16",
        "lineItems": [
            {
                "qty": 1,
                "price": 55000
            }
        ]
    }
```
- The result will be `200 OK`

13. You stop the `InventoryService app` and fire the `POST` request
14. You should be seeing the result from the fallback implementation method

