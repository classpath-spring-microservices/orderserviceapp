package com.synechron.springbootdemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ServiceConfiguration {
    @Value("${spring.cloud.config.uri}")
    private String customMessage;

    public String getCustomMessage() {
        return this.customMessage;
    }
}