package com.synechron.springbootdemo.service;

import com.netflix.appinfo.InstanceInfo;
import com.synechron.springbootdemo.dao.OrderDAO;
import com.synechron.springbootdemo.dao.OrderRepository;
import com.synechron.springbootdemo.model.Order;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderDAO;

    private DiscoveryClient discoveryClient;

    private RestTemplate restTemplate;

    private InventoryFeignClient inventoryFeignClient;

    @Override
    //@CircuitBreaker(name="orderservice", fallbackMethod = "fallBackImplementation")
     @Retry(name = "orderserviceRetry", fallbackMethod = "fallBackImplementation")
    public Order saveOrder(Order order) {
        System.out.println("Came inside the fetchInventory method :::: ");
        //this.inventoryFeignClient.updateQty();
       // int counter =  this.inventoryFeignClient.fetchItemsCount();
       this.restTemplate.exchange("http://INVENTORYSERVICE/api/v1/inventory", HttpMethod.POST, null, Integer.class);
        ResponseEntity<Integer> response = this.restTemplate.exchange("http://INVENTORYSERVICE/api/v1/inventory", HttpMethod.GET, null, Integer.class);
        log.info(" Fetching the reponse from Inventory service ", response.getBody());
        System.out.println("Response :: " + response.getBody());
        //Fetch the instances of Inventory service and invoke the POST method
      //

           /*
            List<ServiceInstance> inventoryservice = this.discoveryClient.getInstances("INVENTORYSERVICE");if(inventoryservice.size() > 0){
            ServiceInstance serviceInstance = inventoryservice.get(0);
            String uri = serviceInstance.getUri().toString();
            String inventoryServicePath = uri+"/api/v1/inventory";
            this.restTemplate.exchange(inventoryServicePath, HttpMethod.POST, null, Integer.class);
            ResponseEntity<Integer> response = this.restTemplate.exchange(inventoryServicePath, HttpMethod.GET, null, Integer.class);
            log.info(" Fetching the reponse from Inventory service ", response.getBody());
            System.out.println("Response :: " + response.getBody());
            */



        return this.orderDAO.save(order);
    }

    public int invokeInventoryService(){
        this.restTemplate.exchange("http://INVENTORYSERVICE/api/v1/inventory", HttpMethod.POST, null, Integer.class);
        ResponseEntity<Integer> response = this.restTemplate.exchange("http://INVENTORYSERVICE/api/v1/inventory", HttpMethod.GET, null, Integer.class);
        log.info(" Fetching the reponse from Inventory service ", response.getBody());
        System.out.println("Response :: " + response.getBody());
        return 200;
    }

    public int invokeInventoryServiceUsingFeignClient(){
        System.out.println("Came inside the fetchInventory method :::: ");
        this.inventoryFeignClient.updateQty();
        int counter =  this.inventoryFeignClient.fetchItemsCount();
        System.out.println("Fetching the counter value using Feign client ::"+ counter);
        return counter;
    }

    public Order fallBackImplementation(Order order, Throwable throwable){
        System.out.println("Came inside the fallback implementation method ::::");
        return Order.builder().orderDate(LocalDate.now()).totalPrice(25000).build();
    }

    @Override
    public Set<Order> fetchOrders() {
        return new HashSet<>(this.orderDAO.findAll());
    }


    @Override
    public Order fetchOrderByOrderId(long orderId) {
        return this.orderDAO.findById(orderId)
                .orElseThrow(OrderServiceImpl::invalidOrderId);
    }

    private static IllegalArgumentException invalidOrderId() {
        return new IllegalArgumentException("Invalid Order Id");
    }

    @Override
    public void archiveOrderByOrderId(long orderId) {
        this.orderDAO.deleteById(orderId);
    }

}